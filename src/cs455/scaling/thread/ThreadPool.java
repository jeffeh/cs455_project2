package cs455.scaling.thread;

import cs455.scaling.util.BlockingQueue;
import cs455.scaling.util.TaskExecutor;

import java.util.ArrayList;
import java.util.List;

public class ThreadPool implements Runnable{

    private final BlockingQueue blockingQueue;
    private final List<TaskExecutor> taskExecutorList = new ArrayList<TaskExecutor>();
    private int poolSize;



    //pass in blocking queue here too?
    public ThreadPool(int poolSize, BlockingQueue blockingQueue) {
        this.poolSize = poolSize;
        this.blockingQueue = blockingQueue;
        for (int i = 0; i < poolSize; i++) {
            //create num of threads here
//            System.out.println("[ThreadPool] i is: " + i);
            taskExecutorList.add(new TaskExecutor(blockingQueue));

        }
    }

    private void startThreadsInPool() {
        for (int i = 0; i < poolSize; i++) {
//            System.out.println("hi " + (i+1));
            Thread taskExecutorThread = new Thread(taskExecutorList.get(i));
            taskExecutorThread.start();
        }
    }


    @Override
    public void run() {
        System.out.println("[ThreadPool] thread created");
        startThreadsInPool();
    }
}
