package cs455.scaling.thread;

import cs455.scaling.util.BlockingQueue;

//Maintains a list of the work that it needs to perform
//Sends the list of work to threadpool
public class ThreadPoolManager implements Runnable{

    private final BlockingQueue blockingQueue;

    public ThreadPoolManager(BlockingQueue blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        System.out.println("[ThreadPoolManager] thread created");
        for (int i = 0; i < 50; i++) {
            try {
                blockingQueue.enqueue(i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
