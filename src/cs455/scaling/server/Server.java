package cs455.scaling.server;

import cs455.scaling.thread.ThreadPool;
import cs455.scaling.thread.ThreadPoolManager;
import cs455.scaling.util.BlockingQueue;

import java.io.IOException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;

public class Server {

    private final int port;
    private final int poolSize;
    private static final int BUFFER_SIZE = 8000;
    private final ByteBuffer byteBuffer = ByteBuffer.allocate(BUFFER_SIZE);

    private final InetAddress host;
    private ServerSocketChannel ssc;
    private Selector selector;


    //BAD
    private String msg;

    public Server(int port, int poolSize) throws IOException {
        this.port = port;
        this.poolSize = poolSize;
        this.selector = this.initializeSelector();
        this.host = InetAddress.getLocalHost();
    }

    //open selector and serversocketchannel here. set it to nonblocking
    //configure inetsocketaddress, bind to address and port, and register serversocketchannel to accept connections
    private Selector initializeSelector() throws IOException {
        Selector socketSelector = SelectorProvider.provider().openSelector();
        ssc = ServerSocketChannel.open();
        ssc.configureBlocking(false);
        InetSocketAddress isa = new InetSocketAddress(this.host, this.port);
        ssc.socket().bind(isa);
        ssc.register(socketSelector, SelectionKey.OP_ACCEPT);

        return socketSelector;
    }

    private static void println(String s) {
        System.out.println(s);
    }

    private void startPoolAndManager() {

        BlockingQueue blockingQueue = new BlockingQueue(poolSize);

        //spawn 2 threads: threadpool and threadpoolmanager
        ThreadPool threadPool = new ThreadPool(poolSize, blockingQueue);
        Thread poolThread= new Thread(threadPool);
        poolThread.start();

        ThreadPoolManager tpManager = new ThreadPoolManager(blockingQueue);
        Thread managerThread = new Thread(tpManager);
        managerThread.start();

    }

    private void startServer() throws IOException, NoSuchAlgorithmException {
        //keep server running
        while (true) {
            //wait for an event
            int number = selector.select();
            if (number == 0) {
                continue;
            }

            Iterator keyIterator = selector.selectedKeys().iterator();
            while (keyIterator.hasNext()) {
                SelectionKey key = (SelectionKey) keyIterator.next();
                keyIterator.remove();

                if (key.isAcceptable()) {
                    this.accept(key);
                } else if (key.isReadable()) {
                    this.read(key);
                } else if (key.isWritable()) {
                    this.write(key);
                }
            }
        }
    }

    //accept connection and make it nonblocking
    //register new socketchannel to selector, OP_READ will notify when data is waiting to be read
    private void accept(SelectionKey key) throws IOException {
        ServerSocketChannel ssc = (ServerSocketChannel) key.channel();
        SocketChannel socketChannel = ssc.accept();
        socketChannel.configureBlocking(false);
        socketChannel.register(selector, SelectionKey.OP_READ);
//        System.out.println("Connection found: " + socketChannel.getRemoteAddress());
    }

    private void read(SelectionKey key) throws IOException, NoSuchAlgorithmException {
        SocketChannel socketChannel = (SocketChannel) key.channel();

        ByteBuffer byteBuffer = ByteBuffer.allocate(BUFFER_SIZE);

        int read = 0;
        try {
            while (byteBuffer.hasRemaining() && read != -1) {
                read = socketChannel.read(byteBuffer);
//                System.out.println(String.format("read: %d, bool: %d", read, byteBuffer.remaining()));
            }
        } catch (IOException ioe) {
            key.cancel();
            socketChannel.close();
            return;
        }

        if (read == -1) {
            key.channel().close();
            key.cancel();
            return;
        }

        //this below should be a task..
        socketChannel.read(byteBuffer);
        byte[] byteMsgRecv = byteBuffer.array();
        msg = SHA1FromBytes(byteMsgRecv);
        System.out.println("msgRecv: " + msg);
        key.interestOps(SelectionKey.OP_WRITE);
    }

    private void write(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();
        //todo..
        //data stored as byte[]
        byte[] data = msg.getBytes();
        System.out.println("sending: " + msg);
        ByteBuffer byteBuffer = ByteBuffer.wrap(data);
        socketChannel.write(byteBuffer);
        key.interestOps(SelectionKey.OP_READ);
    }

    private String SHA1FromBytes(byte[] data) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA1");
        byte[] hash = digest.digest(data);
        BigInteger hashInt = new BigInteger(1, hash);
        return hashInt.toString(16);
    }

    public static void main(String[] args) throws IOException, NoSuchAlgorithmException {
        int port = Integer.parseInt(args[0]);
        int poolSize = Integer.parseInt(args[1]);

        Server s = new Server(port, poolSize);
        //create threads for threadpool and threadpoomanager. then launch the server
//        s.startPoolAndManager();
        println("starting server");
        s.startServer();


    }

}
