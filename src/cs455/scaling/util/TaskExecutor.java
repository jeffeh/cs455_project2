package cs455.scaling.util;

public class TaskExecutor implements Runnable{

    private final BlockingQueue blockingQueue;

    public TaskExecutor(BlockingQueue blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    @Override
    public void run() {
        try {
            int count = 0;
            while (true) {
//                System.out.println("[TaskExecutor] nice " + Thread.currentThread().getName());
                blockingQueue.dequeue();
                count++;
                Thread.sleep(1000);
//                System.out.println("finished by " + Thread.currentThread().getName() + "\t" + count);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
