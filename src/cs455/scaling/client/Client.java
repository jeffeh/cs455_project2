package cs455.scaling.client;

import java.io.IOException;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.SelectorProvider;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

public class Client {

    private final String host;
    private final int port;
    private final int messageRate;
    private final Selector selector;
    private static final int BUFFER_SIZE = 8000;
    private final LinkedList<String> hashList = new LinkedList<>();

    public Client (String host, int port, int messageRate) throws IOException {
        this.host = host;
        this.port = port;
        this.messageRate = messageRate;
        this.selector = this.initiateSelector();
    }

    private Selector initiateSelector() throws IOException {
        return SelectorProvider.provider().openSelector();
    }

    private void startClient() throws IOException, NoSuchAlgorithmException, InterruptedException {
        System.out.println("connecting to " + host + ":" + port);
        SocketChannel socketChannel = SocketChannel.open();
        socketChannel.configureBlocking(false);
        socketChannel.register(selector, SelectionKey.OP_CONNECT);
        socketChannel.connect(new InetSocketAddress(host, port));

        while (true) {
            int number = selector.select();

            if (number == 0) {
                continue;
            }

            Iterator keyIterator = selector.selectedKeys().iterator();
            while (keyIterator.hasNext()) {
                SelectionKey key = (SelectionKey) keyIterator.next();
                keyIterator.remove();

//                System.out.println("key op is: " + key.interestOps());

                if (!key.isValid()) {
                    continue;
                }

                //deal with events
                if (key.isConnectable()) {
                    this.connect(key);
                } else if (key.isReadable()) {
                    this.read(key);
                } else if (key.isWritable()) {
                    this.write(key);
                }

            }
        }
    }

    private void read(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();

        ByteBuffer byteBuffer = ByteBuffer.allocate(BUFFER_SIZE);

        socketChannel.read(byteBuffer);
        String msg = new String(byteBuffer.array()).trim();
        System.out.println("recving: " + msg);
        if (hashList.contains(msg)) {
            System.out.println("found hash");
            System.out.println();
            hashList.remove(msg);
        }
        key.interestOps(SelectionKey.OP_WRITE);
    }

    private void write(SelectionKey key) throws IOException, InterruptedException, NoSuchAlgorithmException {
        SocketChannel socketChannel = (SocketChannel) key.channel();
        ByteBuffer byteBuffer = ByteBuffer.allocate(8000);
        //testing sending a message
//        while (true) {
            byte[] message = new byte[8000];
            new Random().nextBytes(message);
            byteBuffer = ByteBuffer.wrap(message);
            String check = SHA1FromBytes(message);
            hashList.add(check);
            socketChannel.write(byteBuffer);
            System.out.println("sending: " + check);
            byteBuffer.clear();
            Thread.sleep(1000/messageRate);
//        }

        //todo everything under here is test
        key.interestOps(SelectionKey.OP_READ);
    }

    private void connect(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();
        socketChannel.finishConnect();
        key.interestOps(SelectionKey.OP_WRITE);
    }

    private String SHA1FromBytes(byte[] data) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance("SHA1");
        byte[] hash = digest.digest(data);
        BigInteger hashInt = new BigInteger(1, hash);
        return hashInt.toString(16);
    }

    public static void main(String[] args) throws IOException, NoSuchAlgorithmException, InterruptedException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        int messageRate = Integer.parseInt(args[2]);

        System.out.println(String.format("arguments: %s, %d, %d", host, port, messageRate));

        Client client = new Client(host, port, messageRate);

        client.startClient();

    }

}
